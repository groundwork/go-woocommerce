<?php
/*
Plugin Name: GO-WooCommerce
Plugin URI: http://bitbucket.org/goworks/GO
Description: GO Customization for WooCommerce
*/


/* Replace Grouped Product Item Location */
/*   by default the grouped add to cart items appear under the product summary and don't span the
/*   entire page width making the formating difficult.  We'll replace the hook for this
/*   so we can format a full page width column rather than one column of grouped products.
/*   Originally done for the monthly donation page.
*/
add_action('plugins_loaded', 'go_woocommerce_plugins_loaded');

function go_woocommerce_plugins_loaded() {
	remove_action( 'woocommerce_grouped_add_to_cart', 'woocommerce_grouped_add_to_cart', 30 ); 
	add_action( 'woocommerce_after_single_product_summary', 'go_woocommerce_grouped_add_to_cart', 5 );
}

function go_woocommerce_grouped_add_to_cart() {
	$path = plugin_dir_path(__FILE__).'/templates/';
	woocommerce_get_template( 'single-product/add-to-cart/grouped.php', array(), $path, $path );
}
?>
